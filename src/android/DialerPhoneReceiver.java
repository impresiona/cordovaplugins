package com.impresiona.net.cordova.dialer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

public class DialerPhoneReceiver extends BroadcastReceiver{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		Bundle extras = intent.getExtras();
		if(extras != null)
		{
			String state = extras.getString(TelephonyManager.EXTRA_STATE);
			if(state.equals(TelephonyManager.EXTRA_STATE_RINGING))
			{
				String phoneNumber = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
				if(phoneNumber.endsWith("910861591"))
				{
					answerWithBluetoothKey(context);
				}
			}
		}
	}
	
	private void answerWithBluetoothKey(Context context)
	{
		// Simulamos la accion de presionar el boton de responder del manos libres:
        Intent buttonDown = new Intent(Intent.ACTION_MEDIA_BUTTON);             
        buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(buttonDown, "android.permission.CALL_PRIVILEGED");

        // Para froyo y anteriores, el evento es al soltar el boton, no al pulsar:
        Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);               
        buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
        context.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");
	}
}
