package com.impresiona.net.cordova.dialer;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

public class Dialer extends CordovaPlugin {
	public Dialer()
	{
	}
	
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if(action.equals("dial")) {
			this.dial(args.getString(0));
		}else if(action.equals("cancelDial"))
		{
			this.cancelDial();
		}else if(action.equals("toast"))
		{
			this.toast(args.getString(0));
		}else{
			return false;
		}
		callbackContext.success();
		return true;
	}
	
	public void dial(String tel)
	{
		Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tel));
		cordova.getActivity().startActivity(i);
	}
	
	public void cancelDial()
	{
		
	}
	
	public void toast(String text)
	{
		Toast.makeText(this.cordova.getActivity().getApplicationContext(), text, Toast.LENGTH_LONG).show();
	}
}