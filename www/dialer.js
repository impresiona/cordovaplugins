var exec = require('cordova/exec');

module.exports = {

    dial: function(param) {
        if ((typeof param == 'string') && param.length > 0)
		{
            exec(null, null, "Dialer", "dial", [param]);
		}
    },
    toast: function(text) {
        if(typeof text == 'string' && text.length > 0)
		{
			exec(null, null, "Dialer", "toast", [text]);
		}
    },
    cancelDial: function() {
        exec(null, null, "Dialer", "cancelDial", []);
    },
};